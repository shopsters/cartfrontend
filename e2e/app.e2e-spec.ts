import { FrontEndKMN2017Page } from './app.po';

describe('front-end-kmn2017 App', () => {
  let page: FrontEndKMN2017Page;

  beforeEach(() => {
    page = new FrontEndKMN2017Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
