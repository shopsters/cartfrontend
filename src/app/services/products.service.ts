import { Injectable } from '@angular/core';

import { Products } from './products';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ProductsService {


  constructor(private http: Http) {

  }

  getAllProducts(): Promise<Products[]>{
    return this.http.get('http://localhost:3000/products/')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || [];
  }

  private extractImage(res: Response) {
    let body = res.json();
    return body.url || 'assets/images/1.png';
}

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  getImage(pcode): Promise<string> {
    return this.http.get(`http://localhost:3000/productimages/${pcode}`)
      .toPromise()
      .then(this.extractImage)
      .catch(this.handleError);
  }
}
