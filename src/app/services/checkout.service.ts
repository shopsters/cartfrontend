import { Injectable } from '@angular/core';
import {Cart} from './cart';
import {Products} from './products';

@Injectable()
export class CheckoutService {

  constructor() { }

  cart:Cart[] = [];
  getProducts(): Cart[] {
    return this.cart;
  }

  addDummyItem() {
    this.cart.push(new Cart(1, new Products(1, "S1", "350", "dummy Item", "20" , "blah", "blah", "blah")));
  }

  addToCart(product: Products){
    let check = true;
    this.cart.forEach(function(elem){
      if(elem.product.pcode === product.pcode){
        elem.quantity++;
        check = false;
      }
    })
    if(check) {
      this.cart.push(new Cart(1,product));
    }
  }

  removeCartItem(index: number) {
    this.cart.splice(index, 1);
  }

}
