export class Products {

  constructor(
    public id: number,
    public pcode: string,
    public price: string,
    public specification: string,
    public Quantity: string,
    public createdAt: string,
    public updatedAt:  string,
    public imgURL: string)
  {}

}
