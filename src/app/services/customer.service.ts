import { Injectable } from '@angular/core';
import { Customer } from "./customer";
import { LoginComponent } from '../components/login/login.component';
import {Http, Response, RequestOptions, Headers} from "@angular/http";
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class CustomerService
{
  customers : Customer[] = [];

  private customerUrl = "http://localhost:3000/customers";
  loginUrl = "http://localhost:3000/customers/login";
  constructor(private http:Http){}

  autheticate(email:string, password:string): Promise<any>
  {
    return this.http.post(this.loginUrl, {email:email, password:password}, new Headers({'Content-Type':'application/json'}))
      .toPromise();
  }

  register(Name:string, Address:string, phno:number, email:string, password:string, gender :string){
    for(let u of this.customers){
      if(u.email==email){
        alert("already registered");
        return false;
      }
    }
    this.customers.push(new Customer(Name, email, password));

    return this.http.post(this.customerUrl, {email:email, password:password}, new Headers({'Content-Type':'application/json'}))
      .toPromise();
  }
  sync(customers:Customer[]){
    this.customers=customers;
  }

  addUser(customer:Customer){
    this.customers.push(customer);
  }
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }


  getUsers(): Observable<Customer[]> {
    return this.http.get(this.customerUrl)
      .map(this.extractArray)
      .catch(this.handleError);
  }
  getUser(id: number): Observable<Customer> {
    return this.http.get(this.customerUrl + '/' + id)
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteUser(id: number) {
    return this.http.delete(this.customerUrl+'/'+id)
      .toPromise();
  }

  private extractArray(res: Response): any {
    let body = res.json();
    return body || {};
  }

  private extractData(res: Response): any {
    let body = res.json();
    return body || {};
  }
}
