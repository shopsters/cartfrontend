import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../../services/products.service';
import {Products} from '../../services/products';
import {getResponseURL} from '@angular/http/src/http_utils';
import {CheckoutService} from '../../services/checkout.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private PS: ProductsService, private CS: CheckoutService) {
    this.getProducts();

  }

  products: Products[] = [];
  private errorMessage: any = '';

  ngOnInit() {

  }

  getProductImage(pcode: string) {
    this.PS.getImage(pcode).then(
      url => {return url},
      error => this.errorMessage = <any>error
    );
  }

  getProducts() {
    this.PS.getAllProducts().then(
      products => this.products = products,
      error => this.errorMessage = <any>error
    );
  }

  addProductToCart(product: Products) {
    this.CS.addToCart(product);
  }

  logProducts() {
    console.log(this.CS.getProducts());
  }

}
