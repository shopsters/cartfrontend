import { Component, OnInit } from '@angular/core';
import {CheckoutService} from '../../services/checkout.service';
import {Products} from '../../services/products';
import {Cart} from '../../services/cart';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  cart: Cart[] = [];

  totalCalc: number = 0;

  constructor(private CS: CheckoutService) {
    CS.addDummyItem();
    this.cart = CS.getProducts();
    this.totalCalculation(this.cart);
  }

  ngOnInit() {

  }

  decrementQuantity(index: number) {
    // this.CS.cart.forEach(function(elem) {
    //   if(elem.product.pcode === item.product.pcode) {
    //     item.quantity--;
    // //   }
    // });

    this.CS.cart[index].quantity--;

    this.totalCalculation(this.cart);
  }

  incrementQuantity(index: number) {
    // this.CS.cart.forEach(function(elem) {
    //   if(elem.product.pcode === item.product.pcode) {
    //     item.quantity++;
    //   }
    // });

    this.CS.cart[index].quantity++;

    this.totalCalculation(this.cart);
  }

  removeItem(index: number) {
    this.CS.removeCartItem(index);
    this.cart = this.CS.getProducts();
    this.totalCalculation(this.cart);
  }

  calcProductCost(item: Cart) {
    return parseInt(item.product.price) * item.quantity;

  }

  totalCalculation(cart: Cart[]){
    let calc = 0;
    cart.forEach(function(elem){
      calc = calc + (parseInt(elem.product.price) * elem .quantity);
    });
    this.totalCalc = calc;
  }


}
