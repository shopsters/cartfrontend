import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';
import {routing} from './app.routing';
import {RouterModule} from '@angular/router';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CustomerService } from "./services/customer.service";
import {ProductsService} from './services/products.service';
import {CheckoutService} from './services/checkout.service';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    MyAccountComponent,
    ProductsComponent,
    CartComponent,
    CheckoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [RouterModule,  CustomerService, ProductsService, CheckoutService],
  bootstrap: [AppComponent]
})
export class AppModule { }
