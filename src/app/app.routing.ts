/**
 * Created by HP on 6/20/2017.
 */
import {Routes, RouterModule} from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { ProductsComponent } from './components/products/products.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
const APP_ROUTES: Routes = [
  {
    path: '', redirectTo: '/dashboard', pathMatch: 'full'
  },
  {
    path : 'cart' , component : CartComponent
  },
  {
    path : 'dashboard' , component : DashboardComponent
  },
  {
    path : 'products' , component : ProductsComponent
  },
  {
    path : 'login' , component : LoginComponent
  },
  {
    path: 'register' , component: RegisterComponent
  },
  {
    path: 'my-account' , component: MyAccountComponent
  },
  {
    path: 'checkout' , component: CheckoutComponent
  }

];

export const routing = RouterModule.forRoot(APP_ROUTES);
